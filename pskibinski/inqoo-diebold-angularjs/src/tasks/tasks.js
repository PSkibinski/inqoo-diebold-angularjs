const tasks = angular.module('tasks', []);

tasks.controller('TasksCtrl', ($scope) => {

  $scope.mode = 'show';
  $scope.listMode = 'show';
  $scope.nameSearched = '';
  $scope.newTask = '';
  $scope.count = 0;

  $scope.tasks = [
    {
      id: '123',
      name: 'Task 123',
      completed: true
    },
    {
      id: '124',
      name: 'Task 124',
      completed: false
    },
    {
      id: '125',
      name: 'Task 125',
      completed: false
    },
    {
      id: '126',
      name: 'Task 126',
      completed: true
    },
  ];

  $scope.filteredTasks = [...$scope.tasks];

  $scope.selectedTask = {
    id: '123',
    name: 'Task 123',
    completed: true
  }

  $scope.countCompletedTasks = () => {
    $scope.count = $scope.tasks.reduce((acc, next) => {
      return next.completed ? acc + 1 : acc;
    }, 0);
  }
  $scope.countCompletedTasks();

  $scope.filterByName = () => {
    $scope.filteredTasks.sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()));
  }

  $scope.search = (char) => {
    $scope.filteredTasks = $scope.tasks.filter(item => item.name.toLowerCase().includes(char.toLowerCase()));
  }

  $scope.addTask = (name) => {
    $scope.tasks.push({
      id: $scope.tasks.length,
      name: $scope.newTask,
      completed: false
    });
    $scope.newTask = '';
    $scope.filteredTasks = [...$scope.tasks];
  }

  $scope.delete = (id) => {
    $scope.tasks = $scope.tasks.filter(item => item.id !== id);
    $scope.filteredTasks = [...$scope.tasks];
    $scope.filterByName();
    $scope.countCompletedTasks();
  }

  $scope.edit = () => {
    $scope.mode = 'edit';
    $scope.listMode = 'hide';
    $scope.editTask = {...$scope.selectedTask}
  }

  $scope.save = () => {
    $scope.selectedTask = {...$scope.editTask};
    $scope.listMode = 'show';
    $scope.mode = 'hide';
    $scope.tasks = $scope.tasks.map(item =>  item.id === $scope.selectedTask.id ? $scope.selectedTask : item )
    $scope.search($scope.nameSearched);
    $scope.countCompletedTasks();
  }

  $scope.cancel = () => {
    $scope.mode = 'hide';
    $scope.editTask = {};
    $scope.listMode = 'show';
  }

  $scope.clear = () => {
    $scope.editTask = {};
  }

  $scope.active = (id) => {
    $scope.selectedTask = $scope.tasks.find(task => task.id === id);
  }
})