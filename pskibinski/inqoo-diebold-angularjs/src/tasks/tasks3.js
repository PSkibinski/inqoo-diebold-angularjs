const tasks = angular.module("tasks", []);

tasks.directive("tasksPage", function () {
    return {
        scope: {
            sometextparam: "=sometext",
        },

        template: /* html */ `
        <div class="col-md-12">
            {{sometextparam.title}}  {{sometextparam.content}}
        </div>`,
        controller($scope) {
            $scope.infomessage = {
                title: "Informacja o itemach - ",
                content: "Itemy są podzielone na kategorie",
            };
        },
    };
});
