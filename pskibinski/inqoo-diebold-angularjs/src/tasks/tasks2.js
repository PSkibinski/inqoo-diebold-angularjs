const tasks = angular.module("tasks", []);

tasks.component("tasksPage", {
    bindings: {},
    template: /* html */ `
        <div>{{$ctrl.title}}
            <div class="row">
            <div class="col">
                <tasks-list tasks="$ctrl.tasks" active-task="$ctrl.activeTask($event)" selected="$ctrl.selected" 
                delete="$ctrl.delete($event)">
                </tasks-list>
            </div>
            <div class="col">
                <tasks-details title="'Task Details'"   
                    task-selected="$ctrl.selected" ng-if="$ctrl.selected">
                </tasks-details>
                <edit-form ng-if="$ctrl.selected" save="$ctrl.save($event)" cancel="$ctrl.cancel()" 
                    change-mode="$ctrl.changeMode()"></edit-form>
                <add-form ng-if="$ctrl.saveMode" quit="$ctrl.quit()" add="$ctrl.add($event)"></add-form>
            </div>
        </div>
    </div>`,
    controller() {
        this.selected = null;
        this.saveMode = false;
        this.title = "Tasks";
        this.tasks = [
            { id: 1, name: "task 1" },
            { id: 2, name: "task 2" },
            { id: 3, name: "task 3" },
        ];

        this.getTask = id => this.tasks.find(item => item.id === id);

        this.activeTask = id => {
            this.selected = this.getTask(id);
        };

        this.cancel = () => {
            this.selected = null;
        };

        this.save = name => {
            this.selected.name = name;
            this.selected = null;
        };

        this.changeMode = () => {
            this.saveMode = true;
        };

        this.quit = () => {
            this.saveMode = false;
        };

        this.add = newTask => {
            this.tasks.push({
                id: Math.floor(Math.random() * 1000),
                name: newTask.name,
            });
            this.saveMode = false;
        };

        this.delete = id => {
            this.tasks = this.tasks.filter(item => item.id !== id);
        };
    },
});

tasks.component("tasksList", {
    bindings: {
        tasks: "=",
        activeTask: "&",
        selected: "=",
        delete: "&",
    },
    template: /* html */ `
        <div> List: 
            <div class="list-group" ng-repeat="item in $ctrl.tasks">
                <div class="list-group-item" ng-click="$ctrl.activeTask({$event:item.id})"  
                ng-class="{'active' : $ctrl.selected.id === item.id}">{{$index + 1}}. {{item.name}} 
                <span class="close float-end" ng-click="$ctrl.delete({$event: item.id})">&times;</span></div>
            </div>
        </div>`,
});

tasks.component("tasksDetails", {
    bindings: {
        title: "=",
        taskSelected: "=",
    },
    template: /* html */ `
        <div>
            {{$ctrl.title}} 
            <dl>
                <dt>Name</dt>
                <dd> {{$ctrl.taskSelected.name}} </dd>
            </dl>
        </div> <hr>`,
});

tasks.component("editForm", {
    bindings: {
        taskSelected: "=",
        onSave: "&save",
        onCancel: "&cancel",
        changeMode: "&",
    },
    template: /* html */ `
        <label for="taskName" class="form-label mt-2">Task name</label>
        <input type="tekst" id="taskName" class="form-control" ng-model="$ctrl.draft.name">
        <button class="btn btn-primary mt-2" ng-click="$ctrl.onSave({$event: $ctrl.draft.name})">save</button>
        <button class="btn btn-danger mt-2" ng-click="$ctrl.onCancel()">cancel</button>
        <button class="btn btn-success mt-2" ng-click="$ctrl.changeMode()">add new task</button>
    `,
    controller() {
        this.$onInit = () => {
            this.draft = { ...this.taskSelected };
        };
    },
});

tasks.component("addForm", {
    bindings: {
        onQuit: "&quit",
        onAdd: "&add",
    },
    template: /* html */ `
    <div>
        <label for="newTask" class="form-label mt-2">Task name</label>
        <input type="tekst" id="newTask" class="form-control" ng-model="$ctrl.newTask.name">
        <button class="btn btn-primary mt-2" ng-click="$ctrl.onAdd({$event: $ctrl.newTask})">add</button>
        <button class="btn btn-danger mt-2" ng-click="$ctrl.onQuit()">quit</button>
    </div>
    `,
    controller() {
        this.$onInit = () => {
            this.newTask = {};
        };
    },
});
