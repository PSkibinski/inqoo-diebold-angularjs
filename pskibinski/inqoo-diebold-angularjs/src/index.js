// angular.module('config', []);

// angular.module('myApp', ['config']);

const app = angular
    .module("myApp", ["tasks", "users", "settings"])
    .constant("PAGES", [
        {
            name: "tasks",
            label: "Tasks",
            tpl: "./src/views/tasks-view.tpl.html",
        },
        {
            name: "users",
            label: "Users",
            tpl: "./src/views/users-view.tpl.html",
        },
        {
            name: "settings",
            label: "Settings",
            tpl: "./src/views/settings-view.tpl.html",
        },
    ]);

app.run(function ($rootScope) {});

app.controller("appCtrl", ($scope, PAGES) => {
    $scope.showMode = "showUsers";

    $scope.usersMode = "show";
    $scope.tasksMode = "show";

    $scope.user = { name: "Guest" };
    $scope.title = "MyApp";
    $scope.isLoggedIn = false;

    $scope.login = () => {
        $scope.user.name = "Admin";
        $scope.isLoggedIn = true;
    };

    $scope.logout = () => {
        $scope.user.name = "Guest";
        $scope.isLoggedIn = false;
    };

    $scope.showUsers = () => {
        $scope.showMode = "showUsers";
    };

    $scope.showTasks = () => {
        $scope.showMode = "showTasks";
    };

    $scope.pages = PAGES;

    $scope.currentPage = PAGES[1];

    $scope.goToPage = pageName => {
        $scope.currentPage = $scope.pages.find(item => item.name === pageName);
    };
});

angular.bootstrap(document, ["myApp"]);
