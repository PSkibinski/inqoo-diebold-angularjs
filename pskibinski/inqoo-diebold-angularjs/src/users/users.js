const users = angular.module('users', ['users.service']);

users.controller('UsersCtrl', ($scope, $timeout, UsersService) => {

  const vm = $scope.usersPage = {};
  vm.selectedUser = {};

  $scope.$watchCollection(() => UsersService.users, (newValue, oldValue) => {
    $scope.users = UsersService.getUsers();
  })  

  vm.refresh = () => {
    UsersService.fetchUsers().then(data => vm.users = data);
  }
  vm.refresh();

  vm.selectUser = (id) => {
    UsersService.getUserById(id).then(res => {
      vm.selectedUser = {...res}
    })
  }

  vm.save = ({id, name}) => {
    UsersService.updateUser(id, name).then(() => vm.refresh());
  }

  vm.add = (name, email) => {
    if(vm.form.$invalid) {
      vm.showError("Form has errors!");
      return;
    }
    vm.newUser = {name, email};
    UsersService.addNewUser(vm.newUser).then(() => vm.refresh());
    vm.newUser = null;
  }

  vm.remove = (id) => {
    UsersService.deleteUser(id).then(() => vm.refresh()); 
  }

  vm.showError = (msg) => {
    vm.msg = msg;
    $timeout(() => {
      vm.msg = '';
    }, 2000);
  }
})