const usersService = angular.module('users.service', [])

usersService.service('UsersService', function($http) {

  this.users = [];

  this.fetchUsers = () => {
    return $http.get('http://localhost:3000/users').then(res => this.users = [...res.data]);
  }

  this.getUsers = () => {
    return this.users;
  };

  this.updateUser = (id, name) => {
    editedUser = {id, name};
    return $http.put('http://localhost:3000/users/' + id, {name});
  }

  this.getUserById = (id) => {
    return $http.get('http://localhost:3000/users/' + id).then(res => res.data);
  }

  this.addNewUser = (newUser) => {
    return $http.post('http://localhost:3000/users', newUser);
  }

  this.deleteUser = (id) => {
    return $http.delete('http://localhost:3000/users/' + id);
  }
})