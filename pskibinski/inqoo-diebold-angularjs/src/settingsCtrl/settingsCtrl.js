const settings = angular.module("settings", []);

settings.controller("SettingsCtrl", $scope => {
    const vm = ($scope.settings = {});

    vm.title = "Setting X";
});

settings.directive("appHighlight", PAGES => {
    return {
        restrict: "EACM",

        link(scope, $element, attrs, ctrl) {
            console.log("Hello", $element);

            $element
                .css("color", "red")
                .html(`<p>${scope.settings.title}</p>`)
                .on("click", e =>
                    $element.find("p")[0].classList.toggle("text-success")
                );
        },
    };
});
