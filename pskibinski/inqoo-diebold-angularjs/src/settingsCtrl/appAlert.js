const appAlert = angular.module("settings", []);

appAlert.directive("appAlert", () => {
    return {
        scope: {
            message: "@",
            type: "@",
            onDismiss: "&",
        },
        template: /* html */ `
            <div class="alert alert-{{type}}" ng-if="$ctrl.open">
                <span class="close float-end" ng-click="$ctrl.closeAlert()" >&times;</span>
                <span>{{message}}</span>
            </div>
        `,
        controller($scope) {
            const vm = ($scope.$ctrl = {});
            vm.open = true;
            vm.closeAlert = () => {
                vm.open = false;
                $scope.onDismiss();
            };
        },
    };
});
